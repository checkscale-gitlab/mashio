import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import moment from 'moment';
import { connect } from 'dva';
import {
  List,
  Card,
  Row,
  Col,
  Radio,
  Input,
  Progress,
  Button,
  Icon,
  Dropdown,
  Menu,
  Avatar,
  Modal,
  Form,
  DatePicker,
  Select,
} from 'antd';

import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Result from '@/components/Result';

import styles from '../List.less';

import BatchForm from './BatchForm.js';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const SelectOption = Select.Option;
const { Search, TextArea } = Input;

@connect(({ batches, loading }) => ({
  batches,
  batchesLoading: loading.effects['batches/index'],
  tapsLoading: loading.effects['tatps/index'],
}))
@Form.create()
class BatchList extends PureComponent {
  state = { modalVisible: false, modalDone: false }
  pagination = {
    sort: 'created',
    limit: 10,
    direction: 'desc',
    page: 1
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'batches/index',
      payload: {
        ...this.pagination,
        include: 'fermenters,recipes'
      },
    });
  }

  showModal = () => {
    this.setState({
      modalVisible: true,
      current: undefined,
    });
  };

  showEditModal = item => {
    this.setState({
      modalVisible: true,
      current: item,
    });
  };

  handleSubmitted = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      modalDone: true,
    });
  };

  handleDone = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      modalDone: false,
      modalVisible: false,
    });
  };

  handleCancel = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      modalVisible: false,
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'batches/submit',
      payload: { 
        id,
        delete: true,
        ...this.pagination
      },
    });
  };

  render() {
    // const { enums } = this.props;
    const {
      batches: { batches },
      batchesLoading,
    } = this.props;
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { dispatch } = this.props;
    const editAndDelete = (key, currentItem) => {
      if (key === 'edit') this.showEditModal(currentItem);
      else if (key === 'delete') {
        Modal.confirm({
          title: 'Delete Batch',
          content: 'Are you sure you want to delete this batch?',
          okText: 'Yes',
          cancelText: 'No',
          onOk: () => this.deleteItem(currentItem.id),
        });
      }
    };
    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );

    const extraContent = (
      <div className={styles.extraContent}>
        <RadioGroup defaultValue="all">
          <RadioButton value="all">All</RadioButton>
          <RadioButton value="fermenting">Fermenting</RadioButton>
          <RadioButton value="tapped">Tapped</RadioButton>
          <RadioButton value="empty">Empty</RadioButton>
        </RadioGroup>
        <Search className={styles.extraContentSearch} placeholder="Search" onSearch={() => ({})} />
      </div>
    );
    const list = this;
    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      total: (batches.meta ? batches.meta.record_count : 0 ),
      onChange: function(page, pageSize) {
        list.pagination.page = page;
        list.pagination.limit = pageSize;
        dispatch({
          type: 'batches/fetch',
          payload: {
            ...list.pagination
          },
        });
      }
    };

    const ListContent = ({ data: {attributes: { name, created, fillpercent, statusesText } }}) => (
      <div className={styles.listContent}>
        <div className={styles.listContentItem}>
          <span>Status</span>
          <p>{ statusesText ? statusesText : "Planned" }</p>
        </div>
        <div className={styles.listContentItem}>
          <span>Created</span>
          <p>{moment(created).fromNow()}</p>
        </div>
        <div className={styles.listContentItem}>
          <Progress percent={fillpercent * 100} status="normal" strokeWidth={6} style={{ width: 180 }} />
        </div>
      </div>
    );

    const MoreBtn = props => (
      <Dropdown
        overlay={
          <Menu onClick={({ key }) => editAndDelete(key, props.current)}>
            <Menu.Item key="edit">Edit</Menu.Item>
            <Menu.Item key="delete">Delete</Menu.Item>
          </Menu>
        }
      >
        <a>
          Options <Icon type="down" />
        </a>
      </Dropdown>
    );
    
    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info title="Batches" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Fermenting" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Tapped" value="?" />
              </Col>
            </Row>
          </Card>

          <Card
            className={styles.listCard}
            bordered={false}
            title="Batches"
            style={{ marginTop: 24 }}
            bodyStyle={{ padding: '0 32px 40px 32px' }}
            extra={extraContent}
          >
            <Button
              type="dashed"
              style={{ width: '100%', marginBottom: 8 }}
              icon="plus"
              onClick={this.showModal}
              ref={component => {
                /* eslint-disable */
                this.addBtn = findDOMNode(component);
                /* eslint-enable */
              }}
            >
              Add
            </Button>
            <List
              size="large"
              rowKey="id"
              loading={batchesLoading}
              pagination={paginationProps}
              dataSource={batches}
              renderItem={item => (
                <List.Item
                  actions={[
                    <MoreBtn current={item} />,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar src={item.logo} shape="square" size="large" />}
                    title={<a href={'/batches/view/'+item.id}>{item.attributes.name}</a>}
                    description={item.subDescription}
                  />
                  <ListContent data={item} />
                </List.Item>
              )}
            />
          </Card>
        </div>
        <BatchForm
          batch={this.state.current}
          enums={batches.enums}
          modalVisible={this.state.modalVisible}
          modalDone={this.state.modalDone}
          handleDone={this.handleDone}
          handleCancel={this.handleCancel}
          handleSubmitted={this.handleSubmitted}
        ></BatchForm>
      </PageHeaderWrapper>
    );
  }
}

export default BatchList;
