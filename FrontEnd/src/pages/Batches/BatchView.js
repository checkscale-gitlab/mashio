import React, { Component, Fragment } from 'react';
import Debounce from 'lodash-decorators/debounce';
import Bind from 'lodash-decorators/bind';
import { connect } from 'dva';
import {
  Button,
  Menu,
  Dropdown,
  Icon,
  Row,
  Col,
  Steps,
  Card,
  Popover,
  Badge,
  Table,
  Tooltip,
  Divider,
} from 'antd';
import classNames from 'classnames';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from '../Profile/AdvancedProfile.less';

import BatchForm from './BatchForm.js';

const { Step } = Steps;
const { Description } = DescriptionList;
const ButtonGroup = Button.Group;

const getWindowWidth = () => window.innerWidth || document.documentElement.clientWidth;

const extra = (
  <Row>
    <Col xs={24} sm={12}>
      <div className={styles.textSecondary}>Status</div>
      <div className={styles.heading}>Fermenting</div>
    </Col>
    <Col xs={24} sm={12}>
      <div className={styles.textSecondary}>Current Gravity</div>
      <div className={styles.heading}>1.023</div>
    </Col>
  </Row>
);

const popoverContent = (
  <div style={{ width: 160 }}>
    吴加号
    <span className={styles.textSecondary} style={{ float: 'right' }}>
      <Badge status="default" text={<span style={{ color: 'rgba(0, 0, 0, 0.45)' }}>未响应</span>} />
    </span>
    <div className={styles.textSecondary} style={{ marginTop: 4 }}>
      耗时：2小时25分钟
    </div>
  </div>
);

const customDot = (dot, { status }) =>
  status === 'process' ? (
    <Popover placement="topLeft" arrowPointAtCenter content={popoverContent}>
      {dot}
    </Popover>
  ) : (
    dot
  );

@connect(({ batches, loading }) => ({
  batches,
  loading: loading.effects['batches/view'],
}))
class BatchView extends Component {
  state = {
    operationkey: 'tab1',
    stepDirection: 'horizontal',
    modalVisible: false,
    modalDone: false,
  };

  componentDidMount() {
    const { dispatch, match } = this.props;
    const { params } = match;
    console.log(params)
    dispatch({
      type: 'batches/view',
      payload: {
        id: params.id,
        include: 'fermenters'
      }
    });


    this.setStepDirection();
    window.addEventListener('resize', this.setStepDirection, { passive: true });
  }

  showEditModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  handleSubmitted = () => {
    this.setState({
      modalDone: true,
    });
  };

  handleDone = () => {
    this.setState({
      modalDone: false,
      modalVisible: false,
    });
  };

  handleCancel = () => {
    this.setState({
      modalVisible: false,
    });
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.setStepDirection);
    this.setStepDirection.cancel();
  }

  onOperationTabChange = key => {
    this.setState({ operationkey: key });
  };

  @Bind()
  @Debounce(200)
  setStepDirection() {
    const { stepDirection } = this.state;
    const w = getWindowWidth();
    if (stepDirection !== 'vertical' && w <= 576) {
      this.setState({
        stepDirection: 'vertical',
      });
    } else if (stepDirection !== 'horizontal' && w > 576) {
      this.setState({
        stepDirection: 'horizontal',
      });
    }
  }

  render() {
    const { stepDirection, operationkey } = this.state;
    const {
      batches: { batches },
      loading,
      match,

    } = this.props;
    const { params } = match
    let batch = batches[0]

    if (!batch)
      return null

    const action = (
      <Fragment>
        <ButtonGroup>
          <Button onClick={this.showEditModal}>Edit</Button>
          <Button>Delete</Button>
        </ButtonGroup>
        <Button type="primary">Mark as Packaged</Button>
      </Fragment>
    );
    const description = (
      <DescriptionList 
        className={styles.headerList} 
        size="small" 
        col="2">
        <Description term="Volume">{ batch.volume }</Description>
        <Description term="OG">{ batch.og }</Description>
        <Description term="FG">{ batch.fg} </Description>
        <Description term="Brew Date">{ batch.brew_date }</Description>
        <Description term="Tapped Date">2018-08</Description>
        <Description term="Kicked Date">2018-08</Description>
        <Description term="Fermenter">{ batch.fermenter && batch.fermenter.attributes ? batch.fermenter.attributes.name : '?' }</Description>
      </DescriptionList>
    );

    const brewDateDesc = (
      <div className={styles.stepDescription}>
        <Fragment>
          Brew Date
          <Icon type="calendar-o" style={{ color: '#00A0E9', marginLeft: 8 }} />
        </Fragment>
        <div>{ batch.attributes ? batch.attributes["brew-date"] : '?' }</div>
      </div>
    );
    let load = false
    return (
      <PageHeaderWrapper
        title={( batch.attributes ? batch.attributes.name : "")}
        logo={
          <img alt="" src="https://gw.alipayobjects.com/zos/rmsportal/nxkuOJlFJuAUhzlMTCEe.png" />
        }
        action={action}
        content={description}
        extraContent={extra}
        loading={load}
      >
        <Card title="Batch Status" style={{ marginBottom: 24 }} bordered={false}>
          <Steps direction={stepDirection} progressDot={customDot} current={ batch.attributes ? ( batch.attributes.status ? batch.attributes.status : 0 ) : 0 }>
            <Step title="Planned" />
            <Step title="Brewing" description={brewDateDesc} />
            <Step title="Fermenting" />
            <Step title="Packaged" />
          </Steps>
        </Card>
        <Card title="Brew Log" style={{ marginBottom: 24 }} bordered={false}>
          <Card type="inner">
            <DescriptionList size="small" title="2018-12-12">
              <Description>Brewed</Description>
            </DescriptionList>
            <Divider style={{ margin: '16px 0' }} />
            <DescriptionList size="small" title="2018-12-12">
              <Description>Racked to Secondary</Description>
            </DescriptionList>
          </Card>
        </Card>
        <BatchForm
          batch={batch}
          modalVisible={this.state.modalVisible}
          modalDone={this.state.modalDone}
          handleDone={this.handleDone}
          handleCancel={this.handleCancel}
          handleSubmitted={this.handleSubmitted}
        ></BatchForm>
      </PageHeaderWrapper>
    );
  }
}

export default BatchView;
