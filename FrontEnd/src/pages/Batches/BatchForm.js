import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import {
  Form,
  Input,
  DatePicker,
  Select,
  Button,
  Card,
  InputNumber,
  Radio,
  Icon,
  Tooltip,
  Modal,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Result from '@/components/Result';

import styles from '../List.less';
import moment from 'moment';

const FormItem = Form.Item;
const { Option } = Select;
const { RangePicker } = DatePicker;
const { TextArea } = Input;

@connect(({ loading, taps, fermenters, recipes }) => ({
  taps,
  fermenters,
  recipes,
  submitting: loading.effects['form/submitRegularForm'],
}))
@Form.create()
class BatchForm extends PureComponent {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'fermenters/index',
      payload: {}
    });
    dispatch({
      type: 'recipes/index',
      payload: {}
    });
    dispatch({
      type: 'taps/index',
      payload: {}
    });
  }


  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form, batch, handleDone, handleCancel, handleSubmitted } = this.props;
    const pagination = this.pagination;
    const id = batch ? batch.id : '';

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      handleSubmitted()
      fieldsValue.tapped_date = fieldsValue.tapped_date ? fieldsValue.tapped_date.format("YYYY-MM-DD") + " 00:00:00" : null
      fieldsValue.brew_date = fieldsValue.brew_date ? fieldsValue.brew_date.format("YYYY-MM-DD") + " 00:00:00" : null
      fieldsValue.kicked_date = fieldsValue.kicked_date ? fieldsValue.kicked_date.format("YYYY-MM-DD") + " 00:00:00" : null
      dispatch({
        type: 'batches/submit',
        payload: { 
          id,
          ...fieldsValue,
          ...pagination
        },
      });
    });
  };

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  render() {
    const { 
      batch,
      modalDone,
      form: { getFieldDecorator },
      formLayout,
      modalVisible,
      state,
      handleDone,
      handleCancel,
      fermenters,
      recipes,
      taps,
      enums,
    } = this.props;

    const modalFooter = modalDone
    ? { footer: null, onCancel: handleDone }
    : { okText: 'Ok', onOk: this.handleSubmit, onCancel: handleCancel };

    if (modalDone) {
      return (
        <Modal
          title={modalDone ? null : `${batch ? 'Add' : 'Edit'} Batch`}
          className={styles.standardListForm}
          width={640}
          bodyStyle={modalDone ? { padding: '72px 0' } : { padding: '28px 0 0' }}
          destroyOnClose
          visible={modalVisible}
          {...modalFooter}
        >
          <Result
            type="success"
            title="Success"
            description="Batch created successfully!"
            actions={
              <Button type="primary" onClick={handleDone}>
                Ok
              </Button>
            }
            className={styles.formResult}
          />
        </Modal>
      );
    }
    let fermenterSelects = [];
    if (fermenters) {
      for (let fermenterIndex in fermenters.fermenters) {
        let fermenter = fermenters.fermenters[fermenterIndex]
        if (fermenter.attributes) {
          fermenterSelects.push(<Select.Option key={fermenter.id} value={fermenter.id}>{fermenter.attributes.name}</Select.Option>)
        }
      }
    }

    let recipeSelects = [];
    if (recipes) {
      for (let recipeIndex in recipes.recipes) {
        let recipe = recipes.recipes[recipeIndex]
        if (recipe && recipe.attributes) {
          recipeSelects.push(<Select.Option key={recipe.id} value={recipe.id}>{recipe.attributes.name}</Select.Option>)
        }
      }
    }
  
    let tapsSelects = [];
    if (taps) {
      for (let tapIndex in taps.taps) {
        let tap = taps.taps[tapIndex]
        if (tap && tap.attributes) {
          tapsSelects.push(<Select.Option key={tap.id} value={tap.id}>{tap.attributes.name}</Select.Option>)
        }
      }
    }
    const statuses = enums ? enums.statuses.map((d,idx) => <Option key={idx} value={idx}>{d}</Option>):[];

    return (
      <Modal
        title={modalDone ? null : `${batch ? 'Add' : 'Edit'} Batch`}
        className={styles.standardListForm}
        width={640}
        bodyStyle={modalDone ? { padding: '72px 0' } : { padding: '28px 0 0' }}
        destroyOnClose
        visible={modalVisible}
        {...modalFooter}
      >
        <Form onSubmit={this.handleSubmit}>
          <FormItem label="Recipe" {...this.formLayout}>
            {getFieldDecorator('recipe_id', {
              initialValue: (batch && batch.relationships && batch.relationships.recipe) ? batch.relationships.recipe.data.id : null,
            })(
              <Select
                showSearch
                placeholder="Select a Recipe"
                style={{ width: 200 }}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                optionFilterProp="children"
              >{recipeSelects}</Select>
              )}
          </FormItem>
          <FormItem label="Custom Name" {...this.formLayout}>
            {getFieldDecorator('custom_name', {
              initialValue: (batch ? batch.attributes["custom-name"] : ''),
            })(<Input placeholder="Custom Name" />)}
          </FormItem>

          <FormItem label="Volume" {...this.formLayout}>
            {getFieldDecorator('volume', {
              initialValue: (batch ? batch.attributes.volume : ''),
            })(<Input placeholder="Batch Volume" />)}
          </FormItem>
          <FormItem label="Status" {...this.formLayout}>
            {getFieldDecorator('status', {
              initialValue: (batch && batch.attributes.status ? batch.attributes.status : 0),
            })(
              <Select
                // key={item.id}
                placeholder="Select a Status"
                style={{ width: 200 }}
              >{statuses}</Select>
              )}
          </FormItem>
          <FormItem label="Fermenter" {...this.formLayout}>
            {getFieldDecorator('fermenter_id', {
              initialValue: (batch && batch.relationships && batch.relationships.fermenter) ? batch.relationships.fermenter.data.id : null,
            })(
              <Select
                showSearch
                placeholder="Select a Fermenter"
                style={{ width: 200 }}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                optionFilterProp="children"
              >{fermenterSelects}</Select>
              )}
          </FormItem>
          <FormItem 
            label="Brew Date"
            {...this.formLayout}
          >
            {getFieldDecorator('brew_date', {
              initialValue: batch && batch.attributes["brew-date"] ? moment(batch.attributes["brew-date"].split('T')[0]) : null,
            })(<DatePicker style={{ width: 200 }} />)}
          </FormItem>
          <FormItem 
            label="Tapped Date"
            {...this.formLayout}
          >
            {getFieldDecorator('tapped_date', {
              initialValue: batch && batch.attributes["tapped-date"] ? moment(batch.attributes["tapped-date"].split('T')[0]) : null,
            })(<DatePicker style={{ width: 200 }} />)}
          </FormItem>
          <FormItem 
            label="Kicked Date"
            {...this.formLayout}
          >
            {getFieldDecorator('kicked_date', {
              initialValue: batch && batch.attributes["kicked-date"] ? moment(batch.attributes["kicked-date"].split('T')[0]) : null,
            })(<DatePicker style={{ width: 200 }} />)}
          </FormItem>
          <FormItem label="Tap" {...this.formLayout}>
            {getFieldDecorator('tap_id', {
              initialValue: (batch && batch.relationships && batch.relationships.tap) ? batch.relationships.tap.data.id : null,
            })(
              <Select
                showSearch
                placeholder="Select a Tap"
                style={{ width: 200 }}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                optionFilterProp="children"
              >{tapsSelects}</Select>
              )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default BatchForm;
