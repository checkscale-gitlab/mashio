import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import GlobalFooter from '@/components/GlobalFooter';

const { Footer } = Layout;
const FooterView = () => (
  <Footer style={{ padding: 0 }}>
    <GlobalFooter
      links={[
        {
          key: 'gitlab',
          title: <Icon type="gitlab" />,
          href: 'https://gitlab.com/NickBusey/mashio',
          blankTarget: true,
        },
      ]}
      copyright={
        <Fragment>
          Created by Nick Busey
        </Fragment>
      }
    />
  </Footer>
);
export default FooterView;
