import { recordIndex, removeRecord, addRecord, updateRecord } from '@/services/api';

export default {
  namespace: 'recipe',

  state: {
    recipe: [],
  },

  effects: {
    *index({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'recipes');
      response.data.meta = response.meta;

      const styles = yield call(recordIndex, {
        sort: 'name',
        limit: 1000,
        direction: 'asc',
        page: 1
      }, 'styles');
      response.data.styles = styles;

      yield put({
        type: 'queryList',
        payload: response,
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'recipes');

      const styles = yield call(recordIndex, {
        sort: 'name',
        limit: 1000,
        direction: 'asc',
        page: 1
      }, 'styles');
      response.data.styles = styles;
      yield put({
        type: 'appendList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = (payload.delete == true) ? removeRecord : updateRecord;
      } else {
        callback = addRecord;
      }
      
      const response = yield call(callback, payload, 'recipes'); // post

      // Get record index to overwrite everything.
      // Not the most efficient, could just update the needed record. Works for now though.
      const records = yield call(recordIndex, payload, 'recipes');

      const styles = yield call(recordIndex, {
        sort: 'name',
        limit: 1000,
        direction: 'asc',
        page: 1
      }, 'styles');
      records.data.styles = styles;

      yield put({
        type: 'queryList',
        payload: records,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      if (Array.isArray(action.payload)) {
        action.payload.forEach((item, index) => {
          if (item.relationships) {
            var style = action.payload.styles.data.find(obj => {
              return obj.type === "styles" && obj.id === item.relationships.style.data.id;
            })
            item.style = style;
          }
        });

        return {
          ...state,
          recipe: action.payload,
        };
      } else if (Array.isArray(action.payload.data)) {
        action.payload.data.forEach((item, index) => {
          if (item.relationships) {
            var style = action.payload.data.styles.data.find(obj => {
              return obj.type === "styles" && obj.id === item.relationships.style.data.id;
            })
            item.style = style;
          }
        });
        action.payload.data.meta = action.payload.meta
        action.payload.data.included = action.payload.included
        return {
          ...state,
          recipe: action.payload.data,
        };
      } else {
        return state
      }
    },
    appendList(state, action) {
      return {
        ...state,
        recipe: state.recipe.concat(action.payload),
      };
    },
  },
};
