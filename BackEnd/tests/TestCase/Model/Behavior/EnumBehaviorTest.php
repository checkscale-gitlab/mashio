<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EnumBehavior;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Event\EventList;

/**
 * App\Model\Table\EnumBehavior Test Case
 */
class EnumBehaviorTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EnumBehavior
     */
    public $Batches;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.batches',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->Batches = TableRegistry::get('Batches');
        // enable event tracking
        $this->Batches->getEventManager()->setEventList(new EventList());
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Batches);

        parent::tearDown();
    }

    public function testEnumValueToKey() {
        $this->assertEquals($this->Batches->enumValueToKey('status','Brewing'),1);
    }

    public function testEnumValueToKeyFail() {
      $this->assertEquals($this->Batches->enumValueToKey('statussss','Brewing'),false);
    }

    public function testEnumKeyToValue() {
        $this->assertEquals($this->Batches->enumKeyToValue('status',1),'Brewing');
    }

    public function testEnumKeyToValueFail() {
      $this->assertEquals($this->Batches->enumKeyToValue('statussss',1),false);
    }

}
