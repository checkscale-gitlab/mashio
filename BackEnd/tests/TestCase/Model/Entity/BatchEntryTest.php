<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\BatchEntry;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * App\Model\Entity\BatchEntry Test Case
 */
class BatchEntryTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\BatchEntry
     */
    public $BatchEntry;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Batches = TableRegistry::get('Batches');
        $this->BatchEntries = TableRegistry::get('BatchEntries');

        $this->Recipes = TableRegistry::get('Recipes');
        $this->RecipeEntries = TableRegistry::get('RecipeEntries');

        $this->recipe = $this->Recipes->newEntity([]);
        $this->Recipes->save($this->recipe);

        $this->recipe_entry = $this->RecipeEntries->newEntity([
            'recipe_id' => $this->recipe->id,
            'days' => 2
        ]);
        $this->RecipeEntries->save($this->recipe_entry);

        $this->batch = $this->Batches->newEntity([
            'brew_date' => new Time(),
            'recipe_id' => $this->recipe->id
        ]);
        $this->Batches->save($this->batch);
        
        $this->batch_entry = $this->BatchEntries->find('all',[
            'conditions' => ['batch_id' => $this->batch->id]
        ])->first();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BatchEntry);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testPlannedBatchEntry()
    {
        # Test that displayDate virtual property works with a 'Planned' batch entry
        $twoDays = new Time('2 days');
        $this->assertEquals($twoDays->format('Y-m-d'),$this->batch_entry->displayDate->format('Y-m-d'));
    }

    public function testPastBatchEntry() {
        $twoDaysAgo = new Time('-2 days');
        $this->batch_entry->status = $this->BatchEntries->enumValueToKey('status','Completed');
        $this->batch_entry->completed_date = $twoDaysAgo;
        $this->BatchEntries->save($this->batch_entry);

        $this->assertEquals($twoDaysAgo->format('Y-m-d'),$this->batch_entry->displayDate->format('Y-m-d'));
    }
}
