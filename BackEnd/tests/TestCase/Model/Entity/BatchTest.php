<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Batch;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * App\Model\Entity\Batch Test Case
 */
class BatchTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Batch
     */
    public $Batch;

    public $fixtures = [
        'app.batches',
        'app.recipes',
        'app.recipe_entries',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Batches = TableRegistry::get('Batches');
        $this->Recipes = TableRegistry::get('Recipes');

        $this->dayAgo = new Time('1 day ago');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->Batch = new Batch();
        $this->Batch->brew_date = $this->dayAgo;
        $this->Batch->tapped_date = new Time('1 day');
        $this->Batch->volume = 5;
        $this->Batches->save($this->Batch);
        $this->assertEquals(1,$this->Batch->daysTapped);
        $this->assertEquals(3.23,$this->Batch->estimatedRemaining);
        $this->assertEquals(40,$this->Batch->pints);

        $this->Batch->kicked_date = new Time('2 days');
        $this->Batches->save($this->Batch);
        $this->assertEquals(1,$this->Batch->daysTapped);
    }

    public function testName() {
        $recipe = $this->Recipes->newEntity(['name'=>'test recipe']);
        $this->Recipes->save($recipe);

        $this->Batch = new Batch();
        $this->Batch->brew_date = $this->dayAgo;
        $this->Batch->recipe_id = $recipe->id;
        $this->Batches->save($this->Batch);
        $this->Batch = $this->Batches->get($this->Batch->id,['contain'=>['Recipes']]);

        $this->assertEquals($this->dayAgo->format('m-d').' test recipe',$this->Batch->name);

        $this->Batch->custom_name = "Custom Name";
        $this->Batches->save($this->Batch);

        $this->assertEquals($this->dayAgo->format('m-d').' Custom Name',$this->Batch->name);
    }
}
