<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BatchesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\BatchesController Test Case
 */
class OwnRecordsTraitTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Batches',
        'app.Users',
        'app.recipe_entries',
        'app.recipes',
        'app.batch_entries',
        'app.fermenters',
        'app.measurements',
        'app.taps'
    ];

    public function setUp() {
        parent::setUp();
        $this->Users = TableRegistry::get('Users');
        $user = $this->Users->newEntity([
            'api_token' => 'test',
            'active' => 1,
            'role' => 'admin',
            'username' => 'test',
            'password' => 'test',
        ]);
        $this->Users->save($user);

        $this->Batches = TableRegistry::get('Batches');
        $this->batch = $this->Batches->newEntity([
            'user_id' => $user->id
        ]);
        $this->Batches->save($this->batch);

        $this->configRequest([
            'headers' => [
                'Accept' => 'application/vnd.api+json'
            ]
        ]);
        $this->disableErrorHandlerMiddleware();

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => $user->id,
                    'username' => 'testing',
                    // other keys.
                ]
            ]
        ]);
    }

    public function testIndex()
    {
        $result = $this->get('/batches?api_key=test');
        $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $result = $this->get('/batches/view/'.$this->batch->id.'?api_key=test');
        $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/vnd.api+json',
                'Content-Type' => 'application/vnd.api+json'
            ],
            'input' => '{"data":{"type":"batches","attributes":{"name":"test","brew_date":"2019-01-01"}}}'
        ]);
        $this->post('/batches/add?api_key=test');
        $this->assertResponseOk();
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/vnd.api+json',
                'Content-Type' => 'application/vnd.api+json'
            ],
            'input' => '{"data":{"type":"batches","id":"'.$this->batch->id.'","attributes":{"name":"test edit"}}}'
        ]);
        $this->post('/batches/edit/'.$this->batch->id.'?api_key=test');
        $this->assertResponseOk();
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/vnd.api+json',
                'Content-Type' => 'application/vnd.api+json'
            ],
            'input' => '{"data":{"type":"batches","id":"'.$this->batch->id.'"}}'
        ]);
        $this->post('/batches/delete/'.$this->batch->id.'?api_key=test');
        $this->assertResponseOk();
    }
}
