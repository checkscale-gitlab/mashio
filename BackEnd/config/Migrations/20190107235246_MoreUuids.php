<?php
use Migrations\AbstractMigration;

class MoreUuids extends AbstractMigration
{

    public function up()
    {

        $this->table('batch_entries')
            ->removeColumn('date')
            ->changeColumn('recipe_entry_id', 'uuid', [
                'default' => '0',
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('batch_id', 'uuid', [
                'default' => '0',
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('fermenter_id', 'uuid', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('batches')
            ->changeColumn('recipe_id', 'uuid', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('fermenters')
            ->changeColumn('batch_id', 'uuid', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('recipe_entries')
            ->removeColumn('order')
            ->changeColumn('recipe_id', 'uuid', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('taps')
            ->changeColumn('batch_id', 'uuid', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('batch_entries')
            ->addColumn('completed_date', 'date', [
                'after' => 'status',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('days', 'integer', [
                'after' => 'fermenter_id',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->addColumn('minutes', 'integer', [
                'after' => 'days',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->addColumn('batch_order', 'integer', [
                'after' => 'minutes',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->addColumn('target_temperature', 'float', [
                'after' => 'batch_order',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();

        $this->table('recipe_entries')
            ->addColumn('recipe_order', 'integer', [
                'after' => 'title',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('batch_entries')
            ->addColumn('date', 'date', [
                'after' => 'status',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->changeColumn('recipe_entry_id', 'integer', [
                'default' => '0',
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('batch_id', 'integer', [
                'default' => '0',
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('fermenter_id', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->removeColumn('completed_date')
            ->removeColumn('days')
            ->removeColumn('minutes')
            ->removeColumn('batch_order')
            ->removeColumn('target_temperature')
            ->update();

        $this->table('batches')
            ->changeColumn('recipe_id', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();

        $this->table('fermenters')
            ->changeColumn('batch_id', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();

        $this->table('recipe_entries')
            ->addColumn('order', 'integer', [
                'after' => 'title',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('recipe_id', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->removeColumn('recipe_order')
            ->update();

        $this->table('taps')
            ->changeColumn('batch_id', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }
}

