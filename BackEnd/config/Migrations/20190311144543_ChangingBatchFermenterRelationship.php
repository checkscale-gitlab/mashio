<?php
use Migrations\AbstractMigration;

class ChangingBatchFermenterRelationship extends AbstractMigration
{

    public function up()
    {

        $this->table('fermenters')
            ->removeColumn('batch_id')
            ->update();

        $this->table('batches')
            ->addColumn('fermenter_id', 'uuid', [
                'after' => 'kicked_date',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('batches')
            ->removeColumn('fermenter_id')
            ->update();

        $this->table('fermenters')
            ->addColumn('batch_id', 'uuid', [
                'after' => 'capacity',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }
}

