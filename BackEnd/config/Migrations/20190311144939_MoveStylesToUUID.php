<?php
use Migrations\AbstractMigration;

class MoveStylesToUUID extends AbstractMigration
{

    public function up()
    {

        $this->table('styles')
            ->changeColumn('id', 'uuid', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('styles')
            ->changeColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'length' => 11,
                'null' => false,
            ])
            ->update();
    }
}

