<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Yeast Entity
 *
 * @property int $id
 * @property string $name
 * @property string $lab
 * @property string $product_id
 * @property string $style
 * @property string $floculation
 * @property string $temp
 * @property string $notes
 *
 * @property \App\Model\Entity\Product $product
 */
class Yeast extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'lab' => true,
        'product_id' => true,
        'style' => true,
        'floculation' => true,
        'temp' => true,
        'notes' => true,
        'product' => true
    ];
}
