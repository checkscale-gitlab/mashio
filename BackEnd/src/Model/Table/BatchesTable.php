<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Date;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

/**
 * Batches Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 * @property \App\Model\Table\BatchEntriesTable|\Cake\ORM\Association\HasMany $BatchEntries
 * @property |\Cake\ORM\Association\HasMany $Fermenters
 * @property \App\Model\Table\MeasurementsTable|\Cake\ORM\Association\HasMany $Measurements
 *
 * @method \App\Model\Entity\Batch get($primaryKey, $options = [])
 * @method \App\Model\Entity\Batch newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Batch[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Batch|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Batch|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Batch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Batch[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Batch findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BatchesTable extends Table
{
    public $enums = array(
        'status' => array(
            'Planned',
            'Brewing',
            'Fermenting',
            'Packaged',
            'Tapped',
            'Kicked',
            'Dumped'
        )
    );
    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('batches');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Recipes', [
            'foreignKey' => 'recipe_id'
            ]
        );
        $this->hasMany(
            'BatchEntries', [
            'foreignKey' => 'batch_id'
            ]
        );
        $this->belongsTo(
            'Fermenters', [
            'foreignKey' => 'fermenter_id'
            ]
        );
        $this->hasMany(
            'Measurements', [
            'foreignKey' => 'batch_id'
            ]
        );

        $this->belongsTo(
            'Taps', [
                'foreignKey' => 'tap_id'
            ]
        );

        $this->addBehavior('Enum');

        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Search.Search');
    }

    /**
     * Default validation rules.
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        // $validator
        //     ->date('brew_date')
        //     ->allowEmpty('brew_date');

        $validator
            ->numeric('last_sg')
            ->allowEmpty('last_sg');

        $validator
            ->numeric('volume')
            ->allowEmpty('volume');

        $validator
            ->numeric('og')
            ->allowEmpty('og');

        $validator
            ->numeric('fg')
            ->allowEmpty('fg');

        $validator
            ->date('package_date')
            ->allowEmpty('package_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));
        return $rules;
    }

    public function beforeSave($event, $batch, $options)
    {
        if ($batch->status == $this->enumValueToKey('status', 'Kicked')) {
            $batch->tap_id = null;
        } else if ($batch->tap_id) {
            $batch->status = $this->enumValueToKey('status', 'Tapped');
        }

        return $batch;
    }

    public function afterSave($event, $batch, $options)
    {
        if ($batch->isNew()) {
            // Create BatchEntries from RecipeEntries
            $RecipeEntries = TableRegistry::get('RecipeEntries');
            $BatchEntries = TableRegistry::get('BatchEntries');
            $recipeEntries = $RecipeEntries->find('all', ['conditions'=>['recipe_id'=>$batch->recipe_id]]);
            foreach ($recipeEntries as $recipeEntry) {
                $batchEntry = $BatchEntries->newEntity(
                    [
                    'recipe_entry_id' => $recipeEntry->id,
                    'batch_id' => $batch->id,
                    'days' => $recipeEntry->days,
                    'minutes' => $recipeEntry->minutes,
                    'target_temperature' => $recipeEntry->target_temperature,
                    'batch_order' => $recipeEntry->recipe_order,
                    ]
                );
                $BatchEntries->save($batchEntry);
            }
        } else {
            $event = new Event(
                'Model.Batch.edited', $this, [
                'batch' => $batch
                ]
            );
            $this->getEventManager()->dispatch($event);
        }
    }
}
