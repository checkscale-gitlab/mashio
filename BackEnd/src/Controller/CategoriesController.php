<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{
    // @codeCoverageIgnoreStart
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                'Crud.Index',
                'Crud.View',
                ],
                'listeners' => [
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
                'Crud.ApiPagination',
                'Crud.Search'
                ]
            ]
        );
        $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }
    // @codeCoverageIgnoreEnd
}
