<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Traits\OwnRecordsTrait;
use App\Controller\Traits\EnumsTrait;

/**
 * Fermenters Controller
 *
 * @property \App\Model\Table\FermentersTable $Fermenters
 *
 * @method \App\Model\Entity\Fermenter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FermentersController extends AppController
{
    // @codeCoverageIgnoreStart
    use OwnRecordsTrait;
    use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
          'Crud.Crud',
          [
            'actions' => [
              'Crud.Index',
              'Crud.View',
              'Crud.Edit',
              'Crud.Add',
              'Crud.Delete'
            ],
            'listeners' => [
              'CrudJsonApi.JsonApi',
              'CrudJsonApi.Pagination',
              'Crud.ApiPagination',
              'Crud.Search'
            ]
          ]
        );
        // $this->Crud->getConfig(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
        $this->Crud->config('listeners.jsonApi.include', [
            'batch',
            'batch.recipe'
          ]);
    }

    public function index()
    {
      $this->Crud->on('beforePaginate', function (\Cake\Event\Event $event) {
        $event->getSubject()->query->contain([
          'Batches',
          'Batches.Recipes'
        ]);
      });
      return $this->Crud->execute();
    }

    public function view()
    {
      $this->Crud->on('beforeFind', function (\Cake\Event\Event $event) {
        $event->getSubject()->query->contain([
          'Batches',
          'Batches.Recipes'
        ]);
      });
      return $this->Crud->execute();
    }

    // @codeCoverageIgnoreEnd
}
