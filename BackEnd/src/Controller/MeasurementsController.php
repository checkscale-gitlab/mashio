<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Measurements Controller
 *
 * @property \App\Model\Table\MeasurementsTable $Measurements
 *
 * @method \App\Model\Entity\Measurement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MeasurementsController extends AppController
{

}
