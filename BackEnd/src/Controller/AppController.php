<?php
// @codeCoverageIgnoreStart
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    use \Crud\Controller\ControllerTrait;

    public $helpers = ['AssetCompress.AssetCompress'];
    
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Articles.title' => 'asc'
        ]
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        // phpinfo(); die();
        parent::initialize();

        $this->loadComponent(
            'RequestHandler',
            [
                'enableBeforeRedirect' => false,
            ]
        );

        $this->loadComponent('Flash');

        $this->loadComponent('CakeDC/Users.UsersAuth');
        $this->Auth->getConfig('storage', 'Memory');
        $this->Auth->getConfig('unauthorizedRedirect', 'false');
        $this->Auth->getConfig('checkAuthIn', 'Controller.initialize');
        // $this->Auth->config('loginAction', false);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        $actions = [
            'register',
            'login'
        ];
        
        if (in_array($this->request->params['action'], $actions)) {
            // for csrf
            $this->eventManager()->off($this->Csrf);
        
            // for security component
            $this->Security->config('unlockedActions', $actions);
        }
    }
}
