<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Traits\EnumsTrait;
use App\Controller\Traits\OwnRecordsTrait;

/**
 * BatchEntries Controller
 *
 * @property \App\Model\Table\BatchEntriesTable $BatchEntries
 *
 * @method \App\Model\Entity\BatchEntry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BatchEntriesController extends AppController
{
    // @codeCoverageIgnoreStart
    use OwnRecordsTrait;
    use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Edit',
                'Crud.Add',
                'Crud.Delete'
                ],
                'listeners' => [
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
                'Crud.ApiPagination',
                'Crud.Search'
                ]
            ]
        );
        $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }
    // @codeCoverageIgnoreEnd
}
