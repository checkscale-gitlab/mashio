<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace App\Controller\Traits;

use Cake\ORM\TableRegistry;

/**
 * Covers registration features and email token validation
 *
 * @property \Cake\Http\ServerRequest $request
 */
trait EnumsTrait
{
    public function enums()
    {
        $this->table = TableRegistry::get($this->modelClass);
        return $this->response->withType("application/json")->withStringBody(json_encode($this->table->enumValues()));
    }
}
