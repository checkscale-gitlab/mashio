<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Traits\EnumsTrait;

/**
 * Grains Controller
 *
 * @property \App\Model\Table\GrainsTable $Grains
 *
 * @method \App\Model\Entity\Grain[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GrainsController extends AppController
{

    // @codeCoverageIgnoreStart
    // use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                    'Crud.Index',
                    'Crud.View',
                    'Crud.Edit',
                    'Crud.Add',
                    'Crud.Delete'
                ],
                'listeners' => [
                    'CrudJsonApi.JsonApi',
                    'CrudJsonApi.Pagination',
                    'Crud.ApiPagination',
                    'Crud.Search'
                ]
            ]
        );
        // $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }
    // @codeCoverageIgnoreEnd
}
