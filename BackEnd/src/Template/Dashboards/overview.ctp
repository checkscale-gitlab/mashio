<div class="col-lg-9 main-chart">
  <div class="row mtbox">
    <div class="col-md-2 col-sm-2 col-md-offset-2 box0">
      <div class="box1">
        <span class="li_pen"></span>
        <h3><?=$recipeCount?></h3>
      </div>
      <p>You have <?=$recipeCount?> recipes.</p>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <div class="box1">
        <span class="li_fire"></span>
        <h3><?=$timesBrewed?></h3>
      </div>
      <p>You have brewed <?=$timesBrewed?> times.</p>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <div class="box1">
        <span class="li_lab"></span>
        <h3><?=$volumeBrewed?></h3>
      </div>
      <p>You have brewed <?=$volumeBrewed?> gallons.</p>
    </div>

    <div class="col-md-2 col-sm-2 box0">
      <div class="box1">
        <span class="li_cup"></span>
        <h3><?=$this->Calculation->getPintsPerGallons($volumeBrewed)?></h3>
      </div>
      <p><?=$this->Calculation->getPintsPerGallons($volumeBrewed)?> pints brewed total.</p>
    </div>
    
    <div class="col-md-2 col-sm-2 box0">
      <div class="box1">
        <span class="li_stack"></span>
        <h3>OK!</h3>
      </div>
      <p>No tasks due today. RDWHAHB.</p>
    </div>

  </div><!-- /row mt -->	
  <div class="row">
    <?php foreach ($taps as $tap) { ?>
      <div class="col-md-3 mb">
        <div class="darkblue-panel pn">
          <div class="darkblue-header">
              <h3><?=$tap->name?></h3>
          </div>
          <p>
            <?php if ($tap->batch) { ?>
              Tapped:<br /><?=$this->Time->timeAgoInWords($tap->batch->tapped_date)?>
            <?php } else { ?>
              Empty
            <?php } ?>
          </p>
          <footer>
            <?php if ($tap->batch) { ?>
              <h6><i class="fa fa-wine-glass-alt"></i> <?=$this->Html->link($tap->batch->name,['controller'=>'Batches','action'=>'view',$tap->batch->id])?></h6>
              <?php if ($tap->batch->og && $tap->batch->fg) { ?>
                <h5><?=$this->Calculation->getABV($tap->batch->og, $tap->batch->fg);?>% ABV</h5>
              <?php } ?>
            <?php } ?>
          </footer>
        </div><!-- /darkblue panel -->
      </div><!-- /col-md-4 -->
    <?php } ?>
  </div>

</div>