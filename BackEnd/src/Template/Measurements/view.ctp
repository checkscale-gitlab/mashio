<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Measurement $measurement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Measurement'), ['action' => 'edit', $measurement->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Measurement'), ['action' => 'delete', $measurement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $measurement->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Measurements'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Measurement'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="measurements view large-9 medium-8 columns content">
    <h3><?= h($measurement->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Batch') ?></th>
            <td><?= $measurement->has('batch') ? $this->Html->link($measurement->batch->id, ['controller' => 'Batches', 'action' => 'view', $measurement->batch->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($measurement->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($measurement->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value') ?></th>
            <td><?= $this->Number->format($measurement->value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($measurement->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($measurement->date) ?></td>
        </tr>
    </table>
</div>
