<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Measurement $measurement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $measurement->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $measurement->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="measurements form large-9 medium-8 columns content">
    <?= $this->Form->create($measurement) ?>
    <fieldset>
        <legend><?= __('Edit Measurement') ?></legend>
        <?php
            echo $this->Form->control('type');
            echo $this->Form->control('value');
            echo $this->Form->control('date', ['empty' => true]);
            echo $this->Form->control('batch_id', ['options' => $batches, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
