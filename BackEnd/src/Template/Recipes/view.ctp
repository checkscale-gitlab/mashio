<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recipe $recipe
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Recipe'), ['action' => 'edit', $recipe->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Recipe'), ['action' => 'delete', $recipe->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recipe->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Recipes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Styles'), ['controller' => 'Styles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Style'), ['controller' => 'Styles', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="recipes view large-9 medium-8 columns content">
    <h3><?= h($recipe->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Style') ?></th>
            <td><?= $recipe->has('style') ? $this->Html->link($recipe->style->name, ['controller' => 'Styles', 'action' => 'view', $recipe->style->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($recipe->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($recipe->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('OG') ?></th>
            <td><?= h($recipe->og) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FG') ?></th>
            <td><?= h($recipe->fg) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($recipe->description)); ?>
    </div>

    <div class="row mt mb">
        <div class="col-md-12">
            <section class="task-panel tasks-widget">
            <div class="panel-heading">
                <div class="pull-left"><h5><i class="fa fa-tasks"></i> Recipe Steps</h5></div>
                <br>
            </div>
                <div class="panel-body">
                    <div class="task-content">
                        <ul id="sortable" class="task-list ui-sortable">
                            <?php foreach ($recipe->recipe_entries as $recipeEntry): ?>

                                <li class="list-danger">
                                    <i class=" fa fa-ellipsis-v"></i>
                                    <div class="task-checkbox">
                                        <input type="checkbox" class="list-child" value="">
                                    </div>
                                    <div class="task-title">
                                        <span class="task-title-sp"><?= $this->Enum->enumKeyToValue('RecipeEntries','type', $recipeEntry->type) ?></span>
                                        <span class="badge bg-warning">
                                            <?php 
                                                if (isset($recipeEntry->days)) {
                                                    if ($recipeEntry->days > 0) {
                                                        echo $recipeEntry->days . " days after Brew Day";
                                                    } else {
                                                        $recipeEntry->days = $recipeEntry->days * -1;
                                                        echo $recipeEntry->days . " days before Brew Day";
                                                    }
                                                } else {
                                                    echo "Brew Day";
                                                }
                                            ?>
                                        </span>
                                        <div class="pull-right hidden-phone">
                                            <button class="btn btn-success btn-xs fa fa-check"></button>
                                            <?= $this->Html->link(__('&nbsp;'), ['controller' => 'RecipeEntries', 'action' => 'edit', $recipeEntry->id],['escape'=>false,'class'=>'btn btn-success btn-xs fa fa-edit']) ?>
                                            <?= $this->Form->postLink(__('&nbsp;'), ['controller' => 'RecipeEntries', 'action' => 'delete', $recipeEntry->id], ['escape'=>false,'confirm' => __('Are you sure you want to delete # {0}?', $recipeEntry->id),'class'=>'btn btn-success btn-xs fa fa-trash']) ?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class=" add-task-row">
                        <div class="recipeEntries form large-9 medium-8 columns content">
                            <?= $this->Form->create($recipeEntry,['url'=>'/recipeEntries/add','type'=>'post']) ?>
                            <fieldset>
                                <legend><?= __('Add Recipe Step') ?></legend>
                                <label for="type">Recipe Step Type</label>
                                <?= $this->Form->select('type', $this->Enum->selectValues('RecipeEntries', 'type'),['empty' => true]);?>
                                <?php
                                    echo $this->Form->control('minutes');
                                    echo $this->Form->control('days');
                                    echo $this->Form->hidden('recipe_id',['value'=>$recipe->id]);
                                    echo $this->Form->control('title');
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success']) ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div><!--/col-md-12 -->
    </div>
        
    <div class="related">
        <h4><?= __('Batches') ?></h4>
        <?php if (!empty($recipe->batches)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Deleted') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Recipe Id') ?></th>
                <th scope="col"><?= __('Last Sg') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($recipe->batches as $batches): ?>
            <tr>
                <td><?= h($batches->id) ?></td>
                <td><?= h($batches->created) ?></td>
                <td><?= h($batches->modified) ?></td>
                <td><?= h($batches->deleted) ?></td>
                <td><?= h($batches->status) ?></td>
                <td><?= h($batches->recipe_id) ?></td>
                <td><?= h($batches->last_sg) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Batches', 'action' => 'view', $batches->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Batches', 'action' => 'edit', $batches->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Batches', 'action' => 'delete', $batches->id], ['confirm' => __('Are you sure you want to delete # {0}?', $batches->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
