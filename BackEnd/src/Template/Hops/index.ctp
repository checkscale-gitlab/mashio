<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hop[]|\Cake\Collection\CollectionInterface $hops
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Hop'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hops index large-9 medium-8 columns content">
    <h3><?= __('Hops') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('origin') ?></th>
                <th scope="col"><?= $this->Paginator->sort('usage') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alpha_acids') ?></th>
                <th scope="col"><?= $this->Paginator->sort('beta_acids') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hops as $hop): ?>
            <tr>
                <td><?= $this->Number->format($hop->id) ?></td>
                <td><?= h($hop->name) ?></td>
                <td><?= $this->Number->format($hop->origin) ?></td>
                <td><?= h($hop->usage) ?></td>
                <td><?= $this->Number->format($hop->alpha_acids) ?></td>
                <td><?= $this->Number->format($hop->beta_acids) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $hop->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $hop->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $hop->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hop->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
