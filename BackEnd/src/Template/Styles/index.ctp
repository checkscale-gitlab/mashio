<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Style[]|\Cake\Collection\CollectionInterface $styles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="styles index large-9 medium-8 columns content">
    <h3><?= __('Styles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bjcp_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('og_min') ?></th>
                <th scope="col"><?= $this->Paginator->sort('og_max') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fg_min') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fg_max') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ibu_min') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ibu_max') ?></th>
                <th scope="col"><?= $this->Paginator->sort('srm_min') ?></th>
                <th scope="col"><?= $this->Paginator->sort('srm_max') ?></th>
                <th scope="col"><?= $this->Paginator->sort('abv_min') ?></th>
                <th scope="col"><?= $this->Paginator->sort('abv_max') ?></th>
                <th scope="col"><?= $this->Paginator->sort('times_brewed') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($styles as $style): ?>
            <tr>
                <td><?= h($style->name) ?></td>
                <td><?= h($style->bjcp_number) ?></td>
                <td><a href="/categories/view/<?=$style->category->id?>"><?= h($style->category->name) ?></a></td>
                <td><?= $this->Number->format($style->og_min,['places'=>3]) ?></td>
                <td><?= $this->Number->format($style->og_max,['places'=>3]) ?></td>
                <td><?= $this->Number->format($style->fg_min,['places'=>3]) ?></td>
                <td><?= $this->Number->format($style->fg_max,['places'=>3]) ?></td>
                <td><?= $this->Number->format($style->ibu_min) ?></td>
                <td><?= $this->Number->format($style->ibu_max) ?></td>
                <td><?= $this->Number->format($style->srm_min) ?></td>
                <td><?= $this->Number->format($style->srm_max) ?></td>
                <td><?= $this->Number->format($style->abv_min,['places'=>1]) ?>%</td>
                <td><?= $this->Number->format($style->abv_max,['places'=>1]) ?>%</td>
                <td><?= $style->times_brewed ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $style->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
