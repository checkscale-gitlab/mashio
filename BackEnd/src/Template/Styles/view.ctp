<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Style $style
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Styles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="styles view large-9 medium-8 columns content">
    <h3><?= h($style->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($style->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bjcp Number') ?></th>
            <td><?= h($style->bjcp_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($style->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Og Min') ?></th>
            <td><?= $this->Number->format($style->og_min) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Og Max') ?></th>
            <td><?= $this->Number->format($style->og_max) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fg Min') ?></th>
            <td><?= $this->Number->format($style->fg_min) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fg Max') ?></th>
            <td><?= $this->Number->format($style->fg_max) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ibu Min') ?></th>
            <td><?= $this->Number->format($style->ibu_min) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ibu Max') ?></th>
            <td><?= $this->Number->format($style->ibu_max) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Srm Min') ?></th>
            <td><?= $this->Number->format($style->srm_min) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Srm Max') ?></th>
            <td><?= $this->Number->format($style->srm_max) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Abv Min') ?></th>
            <td><?= $this->Number->format($style->abv_min) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Abv Max') ?></th>
            <td><?= $this->Number->format($style->abv_max) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($style->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($style->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted') ?></th>
            <td><?= h($style->deleted) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Aroma') ?></h4>
        <?= $this->Text->autoParagraph(h($style->aroma)); ?>
    </div>
    <div class="row">
        <h4><?= __('Appearance') ?></h4>
        <?= $this->Text->autoParagraph(h($style->appearance)); ?>
    </div>
    <div class="row">
        <h4><?= __('Flavor') ?></h4>
        <?= $this->Text->autoParagraph(h($style->flavor)); ?>
    </div>
    <div class="row">
        <h4><?= __('Mouthfeel') ?></h4>
        <?= $this->Text->autoParagraph(h($style->mouthfeel)); ?>
    </div>
    <div class="row">
        <h4><?= __('Overall') ?></h4>
        <?= $this->Text->autoParagraph(h($style->overall)); ?>
    </div>
    <div class="row">
        <h4><?= __('Comments') ?></h4>
        <?= $this->Text->autoParagraph(h($style->comments)); ?>
    </div>
    <div class="row">
        <h4><?= __('Ingredients') ?></h4>
        <?= $this->Text->autoParagraph(h($style->ingredients)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Recipes') ?></h4>
        <?php if (!empty($style->recipes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Deleted') ?></th>
                <th scope="col"><?= __('Style Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($style->recipes as $recipes): ?>
            <tr>
                <td><?= h($recipes->id) ?></td>
                <td><?= h($recipes->created) ?></td>
                <td><?= h($recipes->modified) ?></td>
                <td><?= h($recipes->deleted) ?></td>
                <td><?= h($recipes->style_id) ?></td>
                <td><?= h($recipes->name) ?></td>
                <td><?= h($recipes->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Recipes', 'action' => 'view', $recipes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Recipes', 'action' => 'edit', $recipes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Recipes', 'action' => 'delete', $recipes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recipes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
