<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BatchEntry $batchEntry
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Batch Entry'), ['action' => 'edit', $batchEntry->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Batch Entry'), ['action' => 'delete', $batchEntry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $batchEntry->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Batch Entries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch Entry'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recipe Entries'), ['controller' => 'RecipeEntries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe Entry'), ['controller' => 'RecipeEntries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="batchEntries view large-9 medium-8 columns content">
    <h3><?= h($batchEntry->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($batchEntry->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Recipe Entry') ?></th>
            <td><?= $batchEntry->has('recipe_entry') ? $this->Html->link($batchEntry->recipe_entry->title, ['controller' => 'RecipeEntries', 'action' => 'view', $batchEntry->recipe_entry->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Batch') ?></th>
            <td><?= $batchEntry->has('batch') ? $this->Html->link($batchEntry->batch->id, ['controller' => 'Batches', 'action' => 'view', $batchEntry->batch->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($batchEntry->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($batchEntry->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($batchEntry->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($batchEntry->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted') ?></th>
            <td><?= h($batchEntry->deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($batchEntry->date) ?></td>
        </tr>
    </table>
</div>
