<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BatchEntry[]|\Cake\Collection\CollectionInterface $batchEntries
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Batch Entry'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recipe Entries'), ['controller' => 'RecipeEntries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe Entry'), ['controller' => 'RecipeEntries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="batchEntries index large-9 medium-8 columns content">
    <h3><?= __('Batch Entries') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recipe_entry_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('batch_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($batchEntries as $batchEntry): ?>
            <tr>
                <td><?= $this->Number->format($batchEntry->id) ?></td>
                <td><?= h($batchEntry->created) ?></td>
                <td><?= h($batchEntry->modified) ?></td>
                <td><?= h($batchEntry->deleted) ?></td>
                <td><?= h($batchEntry->title) ?></td>
                <td><?= $batchEntry->has('recipe_entry') ? $this->Html->link($batchEntry->recipe_entry->title, ['controller' => 'RecipeEntries', 'action' => 'view', $batchEntry->recipe_entry->id]) : '' ?></td>
                <td><?= $batchEntry->has('batch') ? $this->Html->link($batchEntry->batch->id, ['controller' => 'Batches', 'action' => 'view', $batchEntry->batch->id]) : '' ?></td>
                <td><?= $this->Number->format($batchEntry->status) ?></td>
                <td><?= h($batchEntry->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $batchEntry->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $batchEntry->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $batchEntry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $batchEntry->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
