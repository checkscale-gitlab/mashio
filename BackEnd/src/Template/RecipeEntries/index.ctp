<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RecipeEntry[]|\Cake\Collection\CollectionInterface $recipeEntries
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Recipe Entry'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="recipeEntries index large-9 medium-8 columns content">
    <h3><?= __('Recipe Entries') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('deleted') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recipe_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('minutes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('days') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($recipeEntries as $recipeEntry): ?>
            <tr>
                <td><?= $this->Number->format($recipeEntry->id) ?></td>
                <td><?= h($recipeEntry->created) ?></td>
                <td><?= h($recipeEntry->modified) ?></td>
                <td><?= h($recipeEntry->deleted) ?></td>
                <td><?= $this->Number->format($recipeEntry->type) ?></td>
                <td><?= $recipeEntry->has('recipe') ? $this->Html->link($recipeEntry->recipe->name, ['controller' => 'Recipes', 'action' => 'view', $recipeEntry->recipe->id]) : '' ?></td>
                <td><?= $this->Number->format($recipeEntry->minutes) ?></td>
                <td><?= $this->Number->format($recipeEntry->days) ?></td>
                <td><?= h($recipeEntry->title) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $recipeEntry->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $recipeEntry->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $recipeEntry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recipeEntry->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
