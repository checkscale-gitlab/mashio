<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Batch[]|\Cake\Collection\CollectionInterface $batches
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Batch Entries'), ['controller' => 'BatchEntries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch Entry'), ['controller' => 'BatchEntries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="batches index large-9 medium-8 columns content">
    <h3><?= __('Batches') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('brew_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('package_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('progress') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('recipe_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_sg') ?></th>
                <th scope="col"><?= $this->Paginator->sort('volume') ?></th>
                <th scope="col"><?= $this->Paginator->sort('og') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fg') ?></th>
                <th scope="col">ABV</th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($batches as $batch): ?>
            <tr>
                <td><?= $this->Html->link(h($batch->name), ['action' => 'view', $batch->id]) ?></td>
                <td><?= h($batch->brew_date) ?></td>
                <td><?= h($batch->package_date) ?></td>
                <td><?= $this->Calculation->getFermentCompletionPercentage($batch)?>%</td>
                <td><?php if ($batch->status)  { 
                    echo $this->Enum->enumKeyToValue('Batches','status',$batch->status);
                 } else {
                     echo "Planned";
                 } ?></td>
                <td><?= $batch->has('recipe') ? $this->Html->link($batch->recipe->name, ['controller' => 'Recipes', 'action' => 'view', $batch->recipe->id]) : '' ?></td>
                <td><?= $this->Number->format($batch->last_sg) ?></td>
                <td><?= $this->Number->format($batch->volume) ?></td>
                <td><?= $this->Number->format($batch->og) ?></td>
                <td><?= $this->Number->format($batch->fg) ?></td>
                <td><?php if ($batch->og && $batch->fg) { echo $this->Calculation->getABV($batch->og, $batch->fg)."%"; } ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $batch->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $batch->id], ['confirm' => __('Are you sure you want to delete # {0}?', $batch->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
