<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fermenter $fermenter
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fermenter'), ['action' => 'edit', $fermenter->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fermenter'), ['action' => 'delete', $fermenter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fermenter->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fermenters'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fermenter'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fermenters view large-9 medium-8 columns content">
    <h3><?= h($fermenter->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($fermenter->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Batch') ?></th>
            <td><?= $fermenter->has('batch') ? $this->Html->link($fermenter->batch->id, ['controller' => 'Batches', 'action' => 'view', $fermenter->batch->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fermenter->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Capacity') ?></th>
            <td><?= $this->Number->format($fermenter->capacity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fermenter->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fermenter->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted') ?></th>
            <td><?= h($fermenter->deleted) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($fermenter->description)); ?>
    </div>
</div>
