<?php
namespace App\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class BatchesListener implements EventListenerInterface
{
    // @codeCoverageIgnoreStart
    public function implementedEvents()
    {
        return array(
            'Model.BatchEntry.completed' => 'handleCompletedBatchEntry',
        );
    }
    // @codeCoverageIgnoreEnd

    public function handleCompletedBatchEntry($event, $entity)
    {
        $this->Batches = TableRegistry::get('Batches');
        $this->BatchEntries = TableRegistry::get('BatchEntries');
        $this->RecipeEntries = TableRegistry::get('RecipeEntries');

        // Update the batch status as needed
        $batch = $this->Batches->get($entity->batch_id);
        switch ($entity->recipe_entry->type) {
        case $this->RecipeEntries->enumValueToKey('type', 'Heat Strike Water'):
            // Brewing has begin, mark the batch as status of Brewing
            $batch->status = $this->Batches->enumValueToKey('status', 'Brewing');
            break;
        case $this->RecipeEntries->enumValueToKey('type', 'Pitch Yeast'):
            // Brew has ended, mark the batch as Fermenting
            $batch->status = $this->Batches->enumValueToKey('status', 'Fermenting');
            break;
        case $this->RecipeEntries->enumValueToKey('type', 'Package'):
            // Brew has ended, mark the batch as Fermenting
            $batch->status = $this->Batches->enumValueToKey('status', 'Packaged');
            break;
        }
        $this->Batches->save($batch);
        return true;
    }
}
